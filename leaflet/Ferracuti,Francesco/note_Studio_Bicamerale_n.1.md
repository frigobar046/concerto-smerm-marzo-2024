In "Studio Bicamerale n.1" la ricerca, dal punto di vista acustico e musicale,  nasce dalla considerazione che molta prassi live electronics consiste nella catena determinata da strumento che suona,
un computer che rielabora tramite processi e la diffusione del suono elaborato tramite altoparlanti.
Inizialmente l’idea che ha prodotto l’elaborazione di questo strumento aumentato, prevedeva di ribaltare tale catena, abolendo il doppio ascolto
che si percepisce quando si è in presenza un brano live: ll suono acustico prodotto dallo strumento e il suono elaborato elettronicamente diffuso dagli altoparlanti.

Durante il processo di elaborazione dello strumento aumentato questa idea iniziale si è ampliata inglobando la ricerca, tramite il computer, di nuovi timbri e tecniche da poter produrre direttamente nello strumento. 
Si è cercato così di generare delle nuove sonorità, impossibili da ottenere con il suono classico dello strumento, in questo caso un flauto, o con la sola sintesi al computer.
Lo strumento tradizionale risulta discretizzato ovvero diviso in frequenze ben distinte secondo una prassi linguistica musicale che permette la facile trascrizione della musica su carta usando una logica matematica facilmente
trasmissibile. La partitura in questo modo contiene tutte le informazioni necessarie e sufficienti alla riproposizione del brano.

Se possiamo definire “astratta” la notazione classica questo prototipo produce una “concretizzazione” dello strumento. Le sonorità ottenute, infatti, non sono trascrivibili in partitura notazionale.
Il reciproco ascolto e la progressiva sintonia tra il flautista e l’interprete live electronics è parte determinante della composizione.
Questo alternarsi di indicazioni e scelte da parte dei due interpreti è paragonabile alla perduta struttura bicamerale della mente umana.
