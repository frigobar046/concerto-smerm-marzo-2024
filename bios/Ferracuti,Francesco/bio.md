Francesco Ferracuti ebbe il suo primo approccio con la musica all'età di 11 anni quando iniziò a studiare chitarra elettrica e proseguí questo studio per tutta l'adolescenza.
Ebbe i primi contatti con la musica elettronica, nel 2018, partecipando al festival "ArteScienza".
Dopo aver conseguito la maturità scientifica, Francesco si iscrisse al Conservatorio di Musica "Santa Cecilia" al triennio di musica elettronica nel 2019 studiando con
i maestri Nicola Bernardini, Pasquale Citera, Giuseppe Silvi, Luigino Pizzaleo. 
Nel 2023 conseguì il diploma di triennio in musica elettronica con uno studio sugli strumenti aumentati che portò, lavorando con i maestri Giuseppe Silvi e Silvia Lanzalone,
alla creazione del suo primo strumento aumentato: "Il Flauto Bicamerale". 
Ha conseguito una borsa di collaborazione presso il Conservatorio di Musica "Santa Cecilia" nel 2022 e ha collaborato anche come assistente tecnico nel 2023  al festival "ArteScienza" presso il "Goethe Institut".
Attualmente sta svolgendo la magistrale al Conservatorio di Musica "Ottorinio Respighi" di Latina con i maestri Silvia Lanzalone e Federico Scalas.
