# De Mattia, Aedi - The Memory's error

Richieste tecniche:
- 8 uscite audio sull'ottofonia disposte ad anaello (cambio palco)
- 2 ingressi audio, microfonato MS

---

Esecutori:
- Clarinettista con clarinetto basso (Alessandro Malcangi)
- Fixed Media (Giulio De Mattia)

# De Mattia, BVC

>Preferito dal compositore da portare in concerto

Richieste tecniche:
- tutto l'impianto della sala (preferibile LCR)
- 4 ingressi
  - 2 microfoni ravvicinati sugli strumenti (CM3, KM184), amplificazione trasparente
  - coppia stereofonica (Superlux) AT, amplificazione trasparente
