# Tedesco, Corps d'Ombre

Richieste tecniche:
- ottofonia
```
Disposizione
      2
  1       3
  4       5
  6       7
      8
```
- S.T.One o tre altoparlanti con canali indipendenti (sul palco), disposizione S.T.One
```
                  Retro del palco

                      Voce
                  ____________ pannello
                     S.T.One
    Sinistra      ____________ pannello     Destra
                     Flauto
                  ____________ pannello

                    Pubblico

```
- 2 radiomicrofoni
- 3 teli o pannelli (trasparenti o bianchi) di almeno 2 metri di altezza per 3 metri di larghezza
```
       __________
      |          |
      | pannello |   
      |__________|/
      /          /
```
- luci come da partitura
- 1 abat jour
- 2/3 bastoncini di incenso ed un accendino antivento (tipo fiamma ossidrica)

---

Esecutori:
- Flauto Basso (Elena D'Alò)
- Voce (Emanuele Gizzi)
- Live Electronics (Davide Tedesco o studente che non ha portato brano del triennio)
